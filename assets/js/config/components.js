// BASE
import MHeader from '@clientComponents/base/MHeader'
import MFooter from '@clientComponents/base/MFooter'
import Home from '@clientComponents/home/Home'
import ToolBox from '@clientComponents/utilities/ToolBox/ToolBox'
import PlatformGetWidget from '@clientComponents/utilities/platformWidget/PlatformGetWidget'
import MDialog from '@clientComponents/utilities/MDialog'

export default {
  MHeader,
  MFooter,
  Home,
  ToolBox,
  PlatformGetWidget,
  MDialog
}